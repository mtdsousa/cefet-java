Java
==================
Sobre o Curso
-------------------
Curso ministrado aos estudantes do segundo período de Engenharia de Computação do Centro Federal de Educação Tecnológica (CEFET) de Minas Gerais.

  * **Professor**: Helder Rodrigues da Costa
  * **Referência**: JAQUES, Patrícia Augustin. [Programação Básica em Java][1]
  * Introdução 
  * **Listas**
    * Lista 1 - Introdução à Linguagem Java
    * Lista 2 - Estruturas Fundamentais de Programação em Java
    * Lista 3 - Fundamentos da Orientação a Objetos
    * Lista 4 - Strings e Arrays

  [1]: http://professor.unisinos.br/pjaques/papers/Programacao%20Basica%20em%20Java_homepage.pdf
