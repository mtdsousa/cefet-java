public abstract class LivroDeLivraria extends Livro {
    protected double preco;
    protected int estoque;

    public LivroDeLivraria(String titulo, String autor, String editora,
            String ISBN) {
        super(titulo, autor, editora, ISBN);
    }

    public abstract boolean venda();

    public boolean emEstoque() {
        return (this.estoque > 0 ? true : false);
    }

    public double getPreco() {
        return this.preco;
    }

    public boolean setPreco(Float preco) {
        if (preco > 0) {
            this.preco = preco;
            return true;
        }
        return false;
    }
}
