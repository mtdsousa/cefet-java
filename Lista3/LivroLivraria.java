import java.util.Date;

public class LivroLivraria extends LivroDeLivraria {

    public LivroLivraria(String titulo, String autor, String editora,
            String ISBN) {
        super(titulo, autor, editora, ISBN);
    }

    public LivroLivraria(String titulo, String autor, String editora,
            String ISBN, double preco, int estoque) {
        super(titulo, autor, editora, ISBN);
        this.preco = preco;
        this.estoque = estoque;
    }

    public LivroLivraria(LivroComum livro, double preco, int estoque) {
        super(livro.getTitulo(), livro.getAutor(), livro.getEditora(), livro
                .getISBN());
        this.preco = preco;
        this.estoque = estoque;
    }

    @Override
    public boolean venda() {
        if (emEstoque()) {
            this.estoque--;
            return true;
        }
        return false;
    }

    public void show() {
        System.out
                .printf("Livro de Livraria: %s\n\tAutor: %s\n\tEditora: %s\n\tISBN: %s\n",
                        getTitulo(), getAutor(), getEditora(), getISBN());
        System.out.printf("\t"
                + (emEstoque() ? "Disponível em estoque" : "Não disponível")
                + "\n\tPreço: " + getPreco());
        System.out.printf("\n\n");
    }

}
