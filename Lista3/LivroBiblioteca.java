import java.util.Date;

public class LivroBiblioteca extends LivroDeBiblioteca {

    public LivroBiblioteca(String titulo, String autor, String editora,
            String ISBN) {
        super(titulo, autor, editora, ISBN);
    }

    public LivroBiblioteca(LivroComum livro) {
        super(livro.getTitulo(), livro.getAutor(), livro.getEditora(), livro
                .getISBN());
    }

    @Override
    public boolean emprestimo(String usuario, Date hoje, Date devolucao) {
        if (disponivel()) {
            altDisp();
            this.usuario = usuario;
            this.dataEmprestimo = hoje;
            this.dataDevolucao = devolucao;
            return true;
        }
        return false;
    }

    @Override
    public boolean renovacao(Date hoje, Date devolucao, String usuario) {
        if (this.usuario == usuario
                && (this.dataDevolucao.compareTo(hoje) >= 0)) {
            this.dataDevolucao = devolucao;
        }
        return false;
    }

    @Override
    public double devolucao(Date hoje) {
        double multa = 0;
        altDisp();
        this.usuario = "";
        if (this.dataDevolucao.compareTo(hoje) < 0) {
            multa = 1.50; // Multa de R$ 1,00
            // TODO: Implementar valor de multa aplicado por dia.
        }
        this.dataDevolucao = null;
        this.dataEmprestimo = null;
        return multa;
    }

    public void show() {
        System.out
                .printf("Livro de Biblioteca: %s\n\tAutor: %s\n\tEditora: %s\n\tISBN: %s\n",
                        getTitulo(), getAutor(), getEditora(), getISBN());
        if (!disponivel()) {
            System.out
                    .printf("\n\tEm empréstimo por %s\n\t\tData de Empréstimo: %s\n\t\tData de Devolução: %s\n",
                            this.usuario, this.dataEmprestimo.toString(),
                            this.dataDevolucao.toString());
        } else {
            System.out.printf("\tDisponível para empréstimo. \n");
        }
        System.out.printf("\n");
    }
}
