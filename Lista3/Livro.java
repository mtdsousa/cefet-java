public abstract class Livro {
    private String titulo;
    private String autor;
    private String editora;
    private String ISBN;

    public Livro(String titulo, String autor, String editora, String ISBN) {
        this.titulo = titulo;
        this.autor = autor;
        this.editora = editora;
        this.ISBN = ISBN;
    }

    public String getTitulo() {
        return this.titulo;
    };

    public boolean setTitulo(String titulo) {
        this.titulo = titulo;
        return true;
    }

    public String getAutor() {
        return this.autor;
    }

    public boolean setAutor(String autor) {
        this.autor = autor;
        return true;
    }

    public String getEditora() {
        return this.editora;
    }

    public boolean setEditora(String editora) {
        this.editora = editora;
        return true;
    }

    public String getISBN() {
        return this.ISBN;
    }

    public boolean setISBN(String ISBN) {
        this.ISBN = ISBN;
        return true;
    }

    public abstract void show();
}
