/* 
 * 1. Crie um modelo Livro que represente os dados básicos de um livro, sem se
 * preocupar com a sua finalidade.
 * 
 * 2. Usando o resultado do exercício anterior como base, crie um modelo 
 * "LivroDeLivraria" que represente os dados básicos de um livro que está à 
 * venda em uma livraria. 
 * 
 * 3. Usando o resultado do modelo "Livro" como base, crie um modelo
 * "LivroDeBiblioteca" que represente os dados básicos de um livro de uma
 * biblioteca, que pode ser emprestado a leitores.
 * 
 * 4. Escreva uma classe "LivroComum" que represente o modelo desenvolvido no
 * exercício 1.
 * 
 * 5. Escreva uma classe "LivroLivraria" que represente o modelo desenvolvido
 * no exercício 2.
 * 
 * 6. Escreva uma classe "LivroBiblioteca" que represente o modelo desenvolvido
 * no exercício 3.
 * 
 * 7. Escreva uma aplicação em Java que demonstre o uso de instâncias das
 * classes "LivroComum", "LivroLivraria" e "LivroBiblioteca".
 *
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

public class ListaIII {
    public static void main(String[] arguments) {
        Calendar c = new GregorianCalendar();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date hoje = c.getTime();

        c.add(Calendar.DATE, +2);
        Date depoisDeAmanha = c.getTime();

        ArrayList<Livro> livros = new ArrayList<Livro>();
        livros.add(new LivroComum("O Mistério do Cinco Estrelas", "Marcos Rey",
                "Ática", "8526009982"));
        livros.add(new LivroComum("The Da Vinci's Code", "Dan Brown",
                "Random House", "0-385-50420-9"));
        livros.add(new LivroLivraria((LivroComum) livros.get(0), 12.90, 1));
        livros.add(new LivroBiblioteca((LivroComum) livros.get(0)));
        livros.add(new LivroBiblioteca((LivroComum) livros.get(1)));

        // Empréstimo de Livro
        LivroBiblioteca manipulado = (LivroBiblioteca) livros.get(4);
        if (!manipulado.emprestimo("201322040303", hoje, depoisDeAmanha)) {
            System.out.printf("Erro: Não foi possível realizar empréstimo.");
        }

        /*
         * for (Livro s : livros){ s.show(); }
         */

        System.out.printf(hoje.toString() + "\n\n");

        Iterator<Livro> itr = livros.iterator();
        while (itr.hasNext()) {
            Livro element = itr.next();
            element.show();
        }

        // Devolução de Livro
        manipulado.devolucao(hoje);

        // Venda de livro
        LivroLivraria item = (LivroLivraria) livros.get(2);
        item.venda();

        System.out.println("---- Após devolução e compra de livro ----");
        itr = livros.iterator();
        while (itr.hasNext()) {
            Livro element = itr.next();
            element.show();
        }
    }
}
