import java.util.Date;

public abstract class LivroDeBiblioteca extends Livro {
    private boolean disponivel = true;
    protected String usuario;
    protected Date dataEmprestimo;
    protected Date dataDevolucao;

    public LivroDeBiblioteca(String titulo, String autor, String editora,
            String ISBN) {
        super(titulo, autor, editora, ISBN);
    }

    public abstract boolean emprestimo(String usuario, Date hoje, Date devolucao);

    public abstract boolean renovacao(Date hoje, Date devolucao, String usuario);

    public abstract double devolucao(Date hoje); // Retorna o valor da multa

    public boolean disponivel() {
        return this.disponivel;
    }

    public void altDisp() {
        this.disponivel = (this.disponivel ? false : true);
    }
}
