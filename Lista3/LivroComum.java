public class LivroComum extends Livro {

    public LivroComum(String titulo, String autor, String editora, String ISBN) {
        super(titulo, autor, editora, ISBN);
    }

    public void show() {
        System.out.printf(
                "Livro: %s\n\tAutor: %s\n\tEditora: %s\n\tISBN: %s\n\n",
                getTitulo(), getAutor(), getEditora(), getISBN());
    }
}
