
import java.util.ArrayList;

public class Empresa {
    String nome;
    CNPJ cnpj;
    ArrayList<Departamento> departamentos = new ArrayList<Departamento>();
    
    public Empresa(String nome, String cnpj){
        this.nome = nome;
        this.cnpj = new CNPJ(cnpj);
    }
    
    public void addDepartamento(String nome){
        for(int i=0;i<this.departamentos.size();i++){  
            if(this.departamentos.get(i).getNome()==nome){
                return;
            }
        }
        departamentos.add(new Departamento(nome));
    }
    
    public Departamento getDepartamento(String nome){
        for(int i=0;i<this.departamentos.size();i++){  
            if(this.departamentos.get(i).getNome()==nome){
                return this.departamentos.get(i);
            }
        }
        return null;
    }
    
    public void transferir(String nome, String departamento, String novo_departamento){
        Departamento from = this.getDepartamento(departamento);
        Departamento to = this.getDepartamento(novo_departamento);
        to.addFuncionario(from.getFuncionario(nome));
        from.removeFuncionario(from.getFuncionario(nome));
    }
    
    public void status(){
        System.out.printf("\nEmpresa\n- Nome: %s\n- CNPJ: %s (%s)\n- Departamentos: \n", this.nome, this.cnpj.imprimeCNPJ(), (this.cnpj.isValid())?"Validado":"Inválido");
        if(this.departamentos.size()==0){
            System.out.printf("\t(Sem departamentos)");
            return;
        }
        for(int i=0; i<this.departamentos.size(); i++){
            this.departamentos.get(i).status();
        }
    }
}
