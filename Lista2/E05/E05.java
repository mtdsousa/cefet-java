/*
 * Crie uma classe que representa um funcionário, 
 * registrando seu nome, salário e data de admissão. 
 * Em seguida, crie uma classe que represente um 
 * departamento de uma empresa, registrando o nome e os 
 * funcionários que nele trabalham (para uso de vetores, 
 * considere um máximo de 100 funcionários). Por fim, crie 
 * uma classe que representa uma empresa, registrando seu 
 * nome, CNPJ e departamentos (considere um máximo de 10 
 * departamentos). Faça um programa que:
 * a) Crie uma empresa;
 * b) Adicione a esta empresa alguns departamentos;
 * c) Adicione aos departamentos alguns funcionários;
 * d) Dê aumento de 10% a todos os funcionários de um 
 *    determinado departamento;
 * e) Transfira um funcionário de um departamento para outro.
 * 
 * É esperado que seu código seja bem encapsulado. Por
 * exemplo, para adicionar um departamento em uma empresa (ou 
 * um funcionário a um departamento), não se deve acessar o 
 * vetor (ou lista) de departamentos diretamente, mas sim ter 
 * um método na classe que representa a empresa para adicionar 
 * um departamento.
 */

public class E05 {
    public static void main(String[] args){
        Empresa Google = new Empresa("Google", "08320442000127");
        Google.addDepartamento("Glass");        
        Google.addDepartamento("Search");
        Google.getDepartamento("Glass").addFuncionario("Larry Page", 50000, "01/01/1999");
        Google.getDepartamento("Search").addFuncionario("Sergey Brin", 50000, "01/01/1999");
        Google.getDepartamento("Glass").addBonus(10);
        Google.transferir("Sergey Brin", "Search", "Glass");
        Google.status();
    }
}
