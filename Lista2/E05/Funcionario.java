
public class Funcionario {
    private String nome;
    private float salario;
    private Data admissao;

    public Funcionario(String nome, float salario, String admissao){
        this.nome = nome;
        this.salario = salario;
        this.admissao = new Data(admissao);
    }
    
    public void addBonus(int porcent){
        this.salario *= (1+(porcent/(float)100));
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void status(){
        System.out.printf("\t\t- Nome: %s\n\t\t- Salário: R$ %s\n\t\t- Admissão: %s\n\n", this.nome, Float.toString(this.salario), this.admissao.getData());
    }

}
