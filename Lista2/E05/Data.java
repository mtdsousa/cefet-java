
public class Data {
    private int dia;
    private int mes;
    private int ano;
    
    public Data(int dia, int mes, int ano){
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
    
    public Data(String data){
        String[] elementos = data.split("/");
        if (elementos.length==0){
            elementos = data.split("-");
        }
        this.dia = Integer.parseInt(elementos[0]);
        this.mes = Integer.parseInt(elementos[1]);
        this.ano = Integer.parseInt(elementos[2]);
    }
    
    public String getData(){
        return Integer.toString(dia)+"/"+Integer.toString(mes)+"/"+Integer.toString(ano);
    }
}
