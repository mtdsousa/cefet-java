
import java.util.ArrayList;

public class Departamento {
    private ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
    private String nome; 
    
    public Departamento(String nome){
        this.nome = nome;
    }
    
    public Departamento(String nome, ArrayList<Funcionario> funcionarios){
        this.funcionarios = funcionarios;
    }
    
    public void addFuncionario(String nome, float salario, String admissao){
        Funcionario a = new Funcionario(nome, salario, admissao);
        this.funcionarios.add(a);
    }
    
    public void addFuncionario(Funcionario funcionario){
        this.funcionarios.add(funcionario);
    }
    
    public void removeFuncionario(String nome){
        for(int i=0; i<this.funcionarios.size(); i++){
            if(this.funcionarios.get(i).getNome()==nome){
                this.funcionarios.remove(i);
            }
        }
    }
    
    public void removeFuncionario(Funcionario funcionario){
        this.funcionarios.remove(funcionario);
    }
    
    public Funcionario getFuncionario(String nome){
        for(int i=0; i<this.funcionarios.size(); i++){
            if(this.funcionarios.get(i).getNome()==nome){
                return this.funcionarios.get(i);
            }
        }
        return null;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void addBonus(int porcent){
        for(int i=0; i<this.funcionarios.size(); i++){
            funcionarios.get(i).addBonus(porcent);
        }
    }
    
    public void status(){
        System.out.printf("\t-Nome: %s\n \t- Funcionários: \n", this.nome);
        if(this.funcionarios.size()==0){
            System.out.printf("\t\t(Sem funcionários)");
            return;
        }
        for(int i=0; i<this.funcionarios.size(); i++){
            this.funcionarios.get(i).status();
        }
    }
}
