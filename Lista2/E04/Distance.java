
public class Distance {
    private double d; 
    
    public Distance(Point a, Point b){
        this.d = Math.sqrt(Math.pow(a.getX()-b.getX(), 2)+Math.pow(a.getY()-b.getY(), 2));
    }
    
    public double getDistance(){
        return this.d;
    }
}

