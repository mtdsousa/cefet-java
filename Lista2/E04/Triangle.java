
public class Triangle {
    private Point a, b, c;
    
    public Triangle(Point a, Point b, Point c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public double getPerimeter(){
        Distance l1 = new Distance(a, b);
        Distance l2 = new Distance(b, c);
        Distance l3 = new Distance(c, a);
        return  l1.getDistance()+l2.getDistance()+l3.getDistance();
    }
}
