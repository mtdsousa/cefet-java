/*
 * Crie uma classe que representa um ponto no plano
 * cartesiano. Em seguida, crie uma classe que representa
 * um triângulo, reusando a classe anterior por composição. 
 * Finalmente, escreva um programa que receba do usuário as 
 * coordenadas dos vértices do triângulo e imprima seu perímetro.
 */

public class E04 {
    public static void main(String[] args){
        if(args.length==6){
            Point a = new Point(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
            Point b = new Point(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
            Point c = new Point(Integer.parseInt(args[4]), Integer.parseInt(args[5]));
            Triangle t = new Triangle(a, b, c);
            System.out.printf("Resultado: \nA: %s\nB: %s\nC: %s\nPerímetro: %.2f", a.getCordinates(), b.getCordinates(), c.getCordinates(), t.getPerimeter());
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
