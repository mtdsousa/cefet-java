/*
 * Faça um programa que receba o nome a idade, o sexo
 * e salário fixo de um funcionário. Mostre o nome e
 * o salário líquido, sendo:
 * Sexo M:
 *   - Idade >=30: abono de 100,00;
 *   - Idade <30: abono de 50,00;
 * Sexo F:
 *   - Idade >=30: abono de 200,00;
 *   - Idade <30: abono de 80,00;
 */

public class E03 {
    public static void main(String[] args){
        if(args.length==4){
            String nome = args[0];
            int idade = Integer.parseInt(args[1]);
            char sexo = args[2].toCharArray()[0];
            float salario = Float.parseFloat(args[3]);
            
            switch(sexo){
            case 'M':
                if(idade>=30){
                    salario+=100;
                }else{
                    salario+=50;
                }
                break;
            case 'F':
                if(idade>=30){
                    salario+=200;
                }else{
                    salario+=80;
                }
                break;
            }
            System.out.printf("Resultado: %s: %.2f\n", nome, salario);
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
