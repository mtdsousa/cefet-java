/*
 * Modifique as classes do exercício anterior, implementando 
 * um método que imprime na tela o som de cada animal criado
 * a partir dessas classes. Por exemplo: um cachorro deve latir, 
 * uma ave deve gralhar, e um peixe não deve fazer barulho.
 */

public class E09 {
    public static void main(String[] args) {
        Animal cao = new Mamifero("Canis lupus familiaris", "latido", "branco");
        Animal gralhaAzul = new Ave("Cyanocorax caeruleus", "gralhado", "azul");
        Animal Dory = new Peixe("Paracanthurus hepatus", "N/A", "azul");
        
        System.out.printf("Resultado:\n1. Cão: %s\n2. Gralha Azul: %s\n3. Dory: %s\n", cao.som(), gralhaAzul.som(), Dory.som());
    }
}
