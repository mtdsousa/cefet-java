/*
 * A concessionária de veículos "CARANGO" está vendendo
 * os seus veículos com desconto. Faça um programa que
 * calcule e exiba o valor do desconto e o valor a ser
 * pago pelo cliente. O desconto deverá ser calculado
 * sobre o valor do veículo de acordo com o combustível
 * (álcool – 25%, gasolina – 21% ou diesel –14%). Com
 * valor do veículo zero encerra entrada de dados.
 * Informe total de desconto e total pago pelos clientes.
 */

import java.util.Scanner;
public class E02 {
    public static void main(String[] args) {
        int c = 0;
        Scanner input = new Scanner(System.in);
        while(true){
            float car = input.nextFloat();
            if(car<=0){
                System.exit(0);
            }
            int comb = input.nextInt();
            switch(comb){
            case 1:
                car *= 0.75;
                break;
            case 2:
                car *= 0.79;
                break;
            case 3:
                car *= 0.86;
                break;
            }
            System.out.printf("Case %d: %.2f\n", ++c, car);
        }
    }

}
