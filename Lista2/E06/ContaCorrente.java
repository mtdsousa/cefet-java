
public class ContaCorrente {
    private float saldo;
    protected float taxa = (float)0.5;
    
    public ContaCorrente(float saldo_inicial){
        this.saldo = saldo_inicial;
    }
    
    public float getSaldo(){
        return saldo;
    }
    
    public boolean debito(float quantia){
        quantia = Math.abs(quantia);
        quantia *= (1+(taxa/(float)100));
        if(this.saldo<quantia){
            return false;
        }
        this.saldo -= quantia;
        return true;
    }
    
}
