/*
 * Crie uma classe para representar uma conta-corrente,
 * com métodos para depositar uma quantia, sacar uma 
 * quantia e obter o saldo. Para cada saque será debitada
 * também uma taxa de operação equivalente à 0,5% do valor 
 * sacado. Crie, em seguida, uma subclasse desta classe 
 * anterior para representar uma conta-corrente de um 
 * cliente especial. Clientes especiais pagam taxas de 
 * operação de apenas 0,1% do valor sacado. Faça testes com
 * as duas classes e verifique seus resultados.
 */

public class E06 {
    public static void main(String[] args) {
        System.out.println("Teste: Cliente comum: ");
        ContaCorrente comum = new ContaCorrente(1000);
        if(!comum.debito(2000)){
            System.out.println("Débito: terminado com erro.");
        }
        if(!comum.debito(100)){
            System.out.println("Débito: terminado com erro.");
        }
        System.out.printf("Salário: %.2f\n", comum.getSaldo());
        
        System.out.printf("\nTeste: Cliente especial: \n");
        ContaCorrenteEspecial prime = new ContaCorrenteEspecial(1000);
        if(!prime.debito(2000)){
            System.out.println("Débito: terminado com erro.");
        }
        if(!prime.debito(100)){
            System.out.println("Débito: terminado com erro.");
        }
        System.out.printf("Salário: %.2f\n", prime.getSaldo());
    }
}
