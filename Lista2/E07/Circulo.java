
public class Circulo implements FormaGeometrica {
    double raio;
    public Circulo(double raio) {
        this.raio = raio;
    }
    
    public double area(){
        return Math.pow(this.raio, 2)*Math.PI;
    }
    
    public double perimetro(){
        return 2*Math.PI*this.raio;
    }
    
    public void show(){
        System.out.printf("- Raio: %.1f\n", this.raio);
    }

}
