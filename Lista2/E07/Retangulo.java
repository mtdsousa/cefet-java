
public class Retangulo extends Quadrilateros implements FormaGeometrica  {
    public Retangulo(double base, double altura) {
        super(altura, altura, base, base);
    }
    
    public double area(){
        return this.l1*this.l3;
    }
    
    public void show(){
        System.out.printf("- Lados: %.1f, %.1f, %.1f, %.1f\n", this.l1, this.l2, this.l3, this.l4);
    }
}
