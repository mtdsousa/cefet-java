
public class Quadrado extends Quadrilateros implements FormaGeometrica{
    public Quadrado(double lado) {
        super(lado, lado, lado, lado);
    }
    
    public double area(){
        return Math.pow(this.l1, 2);
    }
    
    public void show(){
        System.out.printf("- Lado: %.1f\n", this.l1);
    }

}
