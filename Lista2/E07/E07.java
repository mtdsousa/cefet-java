/*
 * Crie a seguinte hierarquia de classes:
 * a) Uma interface para representar qualquer forma 
 *    geométrica, definindo métodos para cálculo do 
 *    perímetro e cálculo da área da forma;
 * b) Uma classe abstrata para representar quadriláteros. 
 *    Seu construtor deve receber os tamanhos dos 4 lados
 *    e o método de cálculo do perímetro já pode ser 
 *    implementado;
 * c) Classes para representar retângulos e quadrados. 
 *    A primeira deve receber o tamanho da base e da altura 
 *    no construtor, enquanto a segunda deve receber apenas
 *    o tamanho do lado;
 * d) Uma classe para representar um círculo. Seu construtor
 *    deve receber o tamanho do raio.
 *
 * No programa principal, crie quadrados, retângulos 
 * e círculos com tamanhos diferentes e armazene num vetor. 
 * Em seguida, imprima os dados (lados ou raio), os perímetros
 * e as áreas de todas as formas.
 */

public class E07 {
    public static void main(String[] args) {
        FormaGeometrica formas[] = {new Quadrado(10), new Quadrado(5), new Retangulo(10, 5), new Retangulo(5, 2.5), new Circulo(1), new Circulo(2)};
        for(int i=0; i<formas.length; i++){
            System.out.printf("Forma %d\n", i+1);
            formas[i].show();
            System.out.printf("- Perímetro: %.1f\n- Área: %.1f\n\n", formas[i].perimetro(), formas[i].area());
        }
    }
}
