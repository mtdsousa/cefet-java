
public interface FormaGeometrica {
    double perimetro();
    double area();
    void show();
}
