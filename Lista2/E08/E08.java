/*
 * Implemente uma superclasse animal, e depois 3 
 * subclasses: Mamímeros, Peixes e Aves. Depois 
 * crie um programa de testes que lhe permita criar
 * instâncias dessas classes.
 */

public class E08 {
    public static void main(String[] args) {
        Animal cao = new Mamifero("Canis lupus familiaris", "latido", "branco");
        Animal gralhaAzul = new Ave("Cyanocorax caeruleus", "gralhado", "azul");
        Animal Dory = new Peixe("Paracanthurus hepatus", "N/A", "azul");
    }
}
