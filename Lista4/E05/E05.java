/*
 * 5. Escreva uma classe chamada "MatrizDeInteiros" que tenha como atributo uma
 * matriz de inteiros e um construtor que receba como parâmetro a ordem da matriz,
 * a instancie e inicialize com zeros.  Acrescente a classe os seguintes métodos:
 *      
 *      a. Um método que receba como parâmetro três números inteiros indicando 
 *      respectivamente linha, coluna e o valor que deve ser armazenado na linha e
 *      coluna indicada. Obs: Caso a linha ou a coluna passadas como parâmetro 
 *      estejam fora da ordem da matriz indique com uma mensagem o erro.
 *      
 *      b. Um método "éQuadrada", que retorna true se a matriz for quadrada 
 *      (isto é, tem o mesmo número de linhas e colunas).
 *      
 *      c. um método total que some todos os valores da matriz retornando o resultado.
 *      
 *      d. um método que receba como parâmetro um determinado valor e retorne a linha
 *      onde o elemento foi encontrado na matriz ou –1 caso contrário.
 */

public class E05 {
    public static void main(String[] args) {
        MatrizDeInteiros matriz = new MatrizDeInteiros(5, 5);
        matriz.setValue(2, 3, 50);
        matriz.setValue(2, 4, 100);
        System.out.printf("Soma: %d.\nElemento 50 encontrado: %d.\nElemento 200 encontrado: %d.",
                matriz.soma(), matriz.busca(50), matriz.busca(200));
    }
}
