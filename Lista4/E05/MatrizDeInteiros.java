import java.util.ArrayList;


public class MatrizDeInteiros {
    int matriz[][];
    int linha, coluna;
    
    public MatrizDeInteiros(int colunas, int linhas){
        this.linha = linhas;
        this.coluna = colunas;
        this.matriz = new int[this.linha][this.coluna];
    }
    
    public boolean setValue(int linha, int coluna, int valor){
        if(linha>this.linha || coluna>this.coluna){
            return false;
        }
        
        linha--;
        coluna--;
        this.matriz[linha][coluna] = valor;
        return true;
    }
    
    public boolean eQuadrada(){
        return (this.linha==this.coluna?true:false);
    }
    
    public int soma(){
        int s = 0;
        for (int[] linha : this.matriz){
            for (int elemento : linha){
                 s += elemento;
            }
        }
        return s;
    }
    
    public ArrayList<Elemento> buscaCompleta(int valor){
        ArrayList<Elemento> resultado = new ArrayList<Elemento>();
        for (int i=0; i<this.matriz.length; i++){
            for(int j=0; j<this.matriz[i].length; j++){
                if(this.matriz[i][j]==valor){
                    resultado.add(new Elemento(i+1, j+1));
                }
            }
        }
        return resultado;
    }
    
    public int busca(int valor){
        int resultado = -1;
        for (int i=0; i<this.matriz.length; i++){
            for(int j=0; j<this.matriz[i].length; j++){
                if(this.matriz[i][j]==valor){
                    resultado = i+1;
                }
            }
        }
        return resultado;
    }
}
