public class Pais {
    private String nome;
    private String moeda;
    private String idioma;
    private float area;

    public float getArea() {
        return this.area;
    }

    public Pais(String nome, String moeda, String idioma, float area) {
        this.nome = nome;
        this.moeda = moeda;
        this.area = area;
        this.idioma = idioma;
    }

    public void show() {
        System.out.printf(
                "\n%s\n\tIdioma principal: %s\n\tMoeda: %s\n\tÁrea: %.2f\n",
                this.nome, this.idioma, this.moeda, this.area);
    }
}
