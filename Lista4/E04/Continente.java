import java.util.ArrayList;


public class Continente {
    private ArrayList<Pais> paises = new ArrayList<Pais>();
    private String nome;
    
    public boolean addPais(Pais pais){
        paises.add(pais);
        return true;
    }
    
    public float getArea(){
        float area = 0;
        for(Pais p : paises) {  
            area += p.getArea();  
        }
        return area;
    }
    
    public void show(){
        for(Pais p : paises) {  
            p.show(); 
        }
    }
}
