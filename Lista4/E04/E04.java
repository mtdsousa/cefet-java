/*
 * 4. Escreva uma classe continente. 
 * Um continente possui um nome e um conjunto de países, que dele fazem parte. 
 * Desenvolva um método que forneça a dimensão total do continente.
 */

public class E04 {
    public static Continente oceania;

    public static void main(String args[]) {
        oceania = new Continente();
        System.out.println("Adicionando países: ");
        oceania.addPais(new Pais("Nova Zelândia", "Dólar da NZ", "Inglês",
                (float) 268680));
        oceania.addPais(new Pais("Austrália", "Dólar Australiano", "Inglês",
                (float) 23654471));
        oceania.addPais(new Pais("Papua-Nova Guiné", "Guina", "Inglês",
                (float) 6187591));
        oceania.show();

        System.out.printf("\nÁrea total: %.2f", oceania.getArea());
    }
}
