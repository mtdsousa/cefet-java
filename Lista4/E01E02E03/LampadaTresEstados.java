/*
 * 2. Imagine uma lâmpada que possa ter três estados: apagada, acesa e meia-luz. 
 * Usando o modelo "Lâmpada" como base, escreva o modelo "LampadaTresEstados".
 * 
 * 3. Inclua, no modelo "Lâmpada", uma operação "estáLigada" que retorne verdadeiro
 * se a lâmpada estiver ligada e falso, caso contrário.
 * 
 */

public abstract class LampadaTresEstados extends Lampada {
    private int estado;

    public abstract int getEstado();

    public abstract int setEstado(int estado);

    public boolean estaLigada() {
        return (this.estado > 0 ? true : false);
    }

}
