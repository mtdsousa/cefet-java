/*
 * 1. Escreva um modelo para representar uma lâmpada que está à venda em um supermercado.
 * Que dados devem ser representados por este modelo?
 * 
 */

public abstract class Lampada {
    private float preco;
    private int estoque;
    private String fabricante;

    public abstract float getPreco();

    public abstract boolean setPreco(float preco);

    public abstract boolean venda(int qtd);

    public abstract String getFabricante();
}
