/*
 * 6. Escreva uma classe chamada “VetorDeChar" que tenha como atributo um vetor
 * de char e um construtor que receba como parâmetro uma frase. No construtor 
 * deve ser passada a frase para o vetor de char. 
 * 
 * Dica: Use o método "toCharArray" da classe String para colocar os elementos
 * da frase no vetor. Exemplo: char vetor[] = frase.toCharArray();
 * 
 * Acrescente os seguintes métodos a classe:
 *      a. um método que retorne o número de vogais existentes na frase.
 *      b. um método que retorne o número palavras iguais na frase.
 */

public class E06 {

    public static void main(String[] args) {
        //Henrik Ibsen quote:
        VetorDeChar vetor = new VetorDeChar(
                "Com o dinheiro podemos comprar muitas coisas, mas não o essencial para nós. Proporciona-nos comida, mas não apetite; remédios, mas não saúde; dias alegres, mas não a felicidade.");
        System.out.printf("\"%s\":\n\nVogais: %d\nPalavras repetidas: %d\n", vetor.getFrase(), vetor.getVogais(), vetor.getPalavrasIguais());
    }
}
