
public class VetorDeChar {
    char frase[];
    public VetorDeChar(String frase) {
           this.frase = frase.toCharArray();
    }
    
    public int getVogais(){
        int numVogais = 0;
        String vogais = "AEIOU";
        for(int i=0; i<this.frase.length; i++){
            if(vogais.indexOf(Character.toUpperCase(this.frase[i]))>=0){
                numVogais++;
            }
        }
        return numVogais;
    }
    
    public int getPalavrasIguais(){
        int numPalavras = 0;
        String frase = new String(this.frase);
        frase.replaceAll(".", "");
        frase.replaceAll(",", "");
        frase.replaceAll(";", "");
        String[]palavras = (frase).split(" ");
        
        for(int i=0; i<palavras.length; i++){
            for(int j=0; j<palavras.length; j++){
                if(j<i && palavras[i].equalsIgnoreCase(palavras[j])){
                    break;
                }else if(j>i && palavras[i].equalsIgnoreCase(palavras[j])){
                    numPalavras++;
                    break;
                }
            }
        }
        
        return numPalavras;
    }
    
    public String getFrase(){
        return new String(this.frase);
    }

}
