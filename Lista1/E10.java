/*
 * Faça um algoritmo que leia dois números e identifique 
 * se são iguais ou diferentes. Caso eles sejam iguais 
 * imprima uma mensagem dizendo que eles são iguais. Caso 
 * sejam diferentes, informe qual número é o maior, e uma 
 * mensagem que são diferentes.
 */

public class E10 {
    public static void main(String[] args) {
        if(args.length==2){
            float a = Float.parseFloat(args[0]);
            float b = Float.parseFloat(args[1]);
            float result = Float.compare(a, b);
            System.out.printf("Resultado: ");
            if(result==0){
                System.out.printf("São iguais.\n");
            }else{
                System.out.printf("São diferentes, ");
                if(result>0) {
                    System.out.printf("%.2f", a);
                }else if(result<0) {
                    System.out.printf("%.2f", b);
                }
                System.out.println(" é o maior.");
            }
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }

}
