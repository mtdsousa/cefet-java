/*
 * Elaborar um algoritmo que efetue a apresentação do 
 * valor da conversão em real (R$) de um valor lido em 
 * dólar (US$). O algoritmo deverá solicitar o valor da 
 * cotação do dólar e também a quantidade de dólares 
 * disponíveis com o usuário.
 */

public class E06 {
    public static void main(String[] args){
        if(args.length==2){
            float cotacao = Float.parseFloat(args[0]);
            float monetario = Float.parseFloat(args[1]);
            System.out.printf("Resultado: R$ %.2f\n", monetario*cotacao);
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
