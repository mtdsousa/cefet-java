/*
 * Faça um algoritmo que receba um número e mostre 
 * uma mensagem caso este número seja maior que 10.
 */

public class E08 {
    public static void main(String[] args) {
        if (args.length==1){
            if(Float.parseFloat(args[0])>10){
                System.out.println("Resultado: Verdadeiro");
            }
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
