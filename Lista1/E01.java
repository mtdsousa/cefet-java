/*
 * Faça um programa que receba dois números e ao final mostre a soma, 
 * subtração, multiplicação e divisão dos números lidos.
 */

public class E01 {
    public static void main(String[] args){
        if(args.length==2){
            float a = Float.parseFloat(args[0]);
            float b = Float.parseFloat(args[1]);
        
            System.out.printf("%.2f + %.2f = %.2f\n%.2f - %.2f = %.2f\n%.2f * %.2f = %.2f\n", a, b, a+b, a, b, a-b, a, b, a*b);
            if(b!=0){
                System.out.println(String.format("%.2f / %.2f = %.2f", a, b, a/b));
            }
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
