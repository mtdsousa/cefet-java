/*
 * Escrever um programa para determinar o consumo médio 
 * de um automóvel sendo fornecida a distância total 
 * percorrida e o total de combustível gasto.
 */

public class E02 {
    public static void main(String[] args) {
        if(args.length==2){
            float distancia = Float.parseFloat(args[0]);
            float combustivel = Float.parseFloat(args[1]);
            if(combustivel>0){
                float consumo = distancia/combustivel;
                System.out.println("Resultado: "+consumo+" km/l");
            }else{
                System.out.println("Erro: Argumentos inválidos");
            }
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }

}
