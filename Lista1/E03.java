/*
 * Escrever um programa que leia o nome de um vendedor, 
 * o seu salário fixo e o total de vendas efetuadas por ele
 * no mês(em dinheiro). Sabendo que esse vendedor ganha 15%
 * de comissão sobre as vendas efetuadas, informar seu nome, 
 * seu salário fixo e seu salário no final do mês.
 */

public class E03 {
    public static void main(String[] args){
        if(args.length==3){
            String nome = args[0];
            float salario = Float.parseFloat(args[1]);
            float vendas = Float.parseFloat(args[2]); 
            double salario_final = salario + (0.15*vendas);
            System.out.printf("Resultado: %s\nSalário fixo: R$ %.2f\nSalário Final: R$ %.2f\n", nome, salario, salario_final);
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
