/*
 * Ler uma temperatura em graus Celsius e apresentá-la 
 * convertida em graus Fahrenheit. 
 * A fórmula de conversão é: F=(9*C+160) / 5, sendo F a 
 * temperatura em Fahrenheit e C a temperatura em Celsius.
 */

public class E05 {
    public static void main(String[] args){
        if(args.length==1){
            float celsius = Float.parseFloat(args[0]);
            float fah = (9*celsius+160)/5;
            System.out.println("Resultado: "+fah);
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
