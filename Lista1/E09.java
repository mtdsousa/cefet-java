/*
 * Escrever um algoritmo que leia o nome e as três 
 * notas obtidas por um aluno durante o semestre. 
 * Calcular a sua média (aritmética), informar o nome 
 * e sua menção aprovado (media >= 7), Reprovado 
 * (media <= 5) e Recuperação (media entre 5.1 a 6.9).
 */

public class E09 {
    public static void main(String[] args) {
        if(args.length==4){
            float a = Float.parseFloat(args[1]);
            float b = Float.parseFloat(args[2]);
            float c = Float.parseFloat(args[3]);
            float media = (a+b+c)/3;
            System.out.printf("Resultado: %s - ", args[0], media);
            if(media>=7){
                System.out.println("Aprovado.");
            }else if(media<=5){
                System.out.println("Reprovado.");
            }else{
                System.out.println("Recuperacao.");
            }
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }

}
