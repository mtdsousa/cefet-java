/*
 * Ler dois valores para as variáveis A e B, e efetuar as 
 * trocas dos valores de forma que a variável A passe a 
 * possuir o valor da variável B e a variável B passe a 
 * possuir o valor da variável A. Apresentar os valores trocados.
 */

public class E04 {
    public static void main(String[] args){
        if (args.length==2){
            int a = Integer.parseInt(args[0]);
            int b = Integer.parseInt(args[1]);
            int tmp = a;
            a = b;
            b = tmp;
            System.out.println("Resultado: \na: "+a+"\nb: "+b);
        }else{
            System.out.println("Erro: Sem argumentos.");
        }
    }
}
