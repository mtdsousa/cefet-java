/*
 * O custo ao consumidor de um carro novo é a soma 
 * do custo de fábrica com a percentagem do distribuidor 
 * e dos impostos (aplicados, primeiro os impostos sobre 
 * o custo de fábrica, e depois a percentagem do distribuidor 
 * sobre o resultado). Supondo que a percentagem do 
 * distribuidor seja de 28% e os impostos 45%. Escrever um 
 * algoritmo que leia o custo de fábrica de um carro e informe 
 * o custo ao consumidor do mesmo.
 */

public class E07 {
    public static void main(String[] args) {
        float impostos = 48; 
        float distribuidor = 28;
        if(args.length==1){
            float carro = Float.parseFloat(args[0]);
            carro += carro*(impostos/100);
            carro += carro*(distribuidor/100);
            System.out.printf("Resultado: R$ %.2f\n", carro);
        }else{
            System.out.println("Erro: Sem argumentos");
        }
    }

}
